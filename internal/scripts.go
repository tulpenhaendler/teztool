package internal

func GetCopierScript() string {
	return `

DIR="/sourcevol/data"
if [ -d "$DIR" ]; then # copying from alphanet.sh volume
  echo "Detected Alphanet.sh volume..."
  cp -r /sourcevol/data/* /targetvol/
else
  cp -r /sourcevol/* /targetvol/
fi
# no longer need old config files
rm /targetvol/config.json > /dev/null 2>&1
rm /targetvol/config > /dev/null 2>&1
chmod -cR 777 /targetvol > /dev/null 2>&1

`
}

func GetImporterScript() string {
	return `

DIR="/importsrc"
if [ -d "$DIR" ]; then
  echo "Importing from folder"
  cp -r /importsrc/* /targetvol/
else
  echo "Importing from .tar.gz archive"
  tar -xzvf /importsrc -C / > /dev/null 2>&1
fi
# no longer need old config files
rm /targetvol/config.json > /dev/null 2>&1
rm /targetvol/config > /dev/null 2>&1
chmod -cR 777 /targetvol > /dev/null 2>&1

`
}
