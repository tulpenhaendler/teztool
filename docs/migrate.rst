Migration Guide
===============

Migrate from Alphanet.sh to teztool
-----------------------------------

This is a Guide for migrating form the alphanet.sh / mainnet.sh script
to teztool

First, make sure your old node is stopped with

::

   $ ./mainnet stop

This script stored the Tezos node and client data on a special docker
volume, named ``<network>_node_data``, to find the name of this volume
run

::

   $ docker volume ls
   local               mainnet_client_data
   local               mainnet_node_data

to import data from this volumes into the teztool volumes, run

::

   $ teztool create <name>
   $ teztool <name> backup import <volumename>
   # for example:
   $ teztool <name> backup import mainnet_node_data

   # optionally, also import the client data ( stored private keys and addresses )
   $ teztool <name> backup import mainnet_client_data

Afterwards, you can start your teztool container normally with

::

   $ teztool create <name>
   $ teztool <name> start
   # then make sure it is running with:

   $ teztool <name> logs

If everything is ok, you can remove the old volumes with:

::

   $ docker volume rm mainnet_node_data

Migrate from local node
-----------------------

If you have a Node running on your Host machine, or a .tar.gz of the
data and client directories, you can import them with

::

   teztool <name> backup import <tar.gz folder or path> # will import tezos-node data
   teztool <name> backup import <tar.gz folder or path> --importClient # will import tezos-client data

Afterwards, you can start your teztool container normally with

::

   $ teztool <name> start
   # then make sure it is running with:

   $ teztool <name> logs
