FROM golang:latest
WORKDIR /app
COPY go.mod go.mod
RUN go mod download
RUN go get -u github.com/gobuffalo/packr/v2/packr2
COPY . .
RUN CGO_ENABLED=0 GOOS=linux packr2 build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o app *.go
RUN chmod +x app

FROM docker:19.03
COPY --from=0 /app/app /teztool
RUN ln -s /usr/local/bin/docker /usr/bin/docker
WORKDIR /mnt/pwd
ENTRYPOINT ["/teztool"]
