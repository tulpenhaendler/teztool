package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/nomadic-labs/teztool/internal"
	"go.uber.org/dig"
)

func GetRMNetworkCommand(c *dig.Container, name string) *cobra.Command {
	var rm = &cobra.Command{
		Use:   "rm",
		Short: "Delete node and related containers",
		Run: func(cmd *cobra.Command, args []string) {

			_ = c.Invoke(func(tool *internal.TezTool, docker *internal.DockerWrapper) {

				tool.RemoveContainers(name)
			})

		},
	}

	return rm
}
